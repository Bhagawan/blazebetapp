package com.example.blazebetapp

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.blazebetapp.ui.screens.Screens
import com.example.blazebetapp.util.*
import com.example.casinogames.util.Navigator
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class BlazeMainViewModel: ViewModel() {
    private var request: Job? = null
    private var mainRequest: Job? = null
    private var gamesRequest: Job? = null
    private val server = BlazeServerClient.create()
    private val gameRequests = ArrayList<Job>()

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = server.getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> getData()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                getData()
                            }
                            else -> viewModelScope.launch {
                                CurrentAppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else getData()
                } else getData()
            }
        } catch (e: Exception) {
            getData()
        }
    }

    private fun getData() {
        gamesRequest = viewModelScope.launch {
            listOf(
            async {
                val list = server.getGamesList(FileFootballList)
                if(list.isSuccessful) {
                    if(list.body() != null) CurrentAppData.footballGames = list.body()!!
                }
            },
            async {
                val list = server.getGamesList(FileBasketballList)
                if(list.isSuccessful) {
                    if(list.body() != null) CurrentAppData.basketballGames = list.body()!!
                }
            },
            async {
                val list = server.getGamesList(FileCricketList)
                if(list.isSuccessful) {
                    if(list.body() != null) CurrentAppData.cricketGames = list.body()!!
                }
            },
            async {
                val list = server.getGamesList(FileRugbyList)
                if(list.isSuccessful) {
                    if(list.body() != null) CurrentAppData.rugbyGames = list.body()!!
                }
            },
            async {
                val list = server.getGamesList(FileBaseballList)
                if(list.isSuccessful) {
                    if(list.body() != null) CurrentAppData.baseballGames = list.body()!!
                }
            }).joinAll()
//            football.join()
//            basketball.join()
//            cricket.join()
//            rugby.join()
//            baseball.join()
            switchToApp()
        }

    }

    private fun check() {
        var ready = true
        for(job in gameRequests) {
            if (job.isActive) ready = false
        }
        if(ready) switchToApp()
    }

    private fun switchToApp() {
        Navigator.navigateTo(Screens.MAIN_SCREEN)
    }
}