package com.example.blazebetapp

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.compose.rememberNavController
import com.example.blazebetapp.ui.theme.BlazeBetAppTheme
import com.example.blazebetapp.util.NavigationComponent
import im.delight.android.webview.AdvancedWebView
import java.util.*

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel = ViewModelProvider(this)[BlazeMainViewModel::class.java]
        if(savedInstanceState == null) viewModel.init((getSystemService(TELEPHONY_SERVICE) as TelephonyManager).simCountryIso, getId())
        setContent {
            val webView = remember { createWebView() }
            val navController = rememberNavController()
            BlazeBetAppTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavigationComponent(navController, webView)
                }
            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun createWebView() : AdvancedWebView {
        return AdvancedWebView(this).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            setCookiesEnabled(true)
            setMixedContentAllowed(true)
            webChromeClient = object: WebChromeClient() {
                private var h = 0
                private var uiVisibility = 0
                private var d: View? = null
                private var a: CustomViewCallback? = null
                override fun getDefaultVideoPoster(): Bitmap? = if(d == null) null else BitmapFactory.decodeResource(resources, R.drawable.ic_baseline_video)
                override fun onHideCustomView() {
                    (window.decorView as FrameLayout).removeView(d)
                    d = null
                    window.decorView.systemUiVisibility = uiVisibility
                    requestedOrientation = h
                    a!!.onCustomViewHidden()
                    a = null
                }

                override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                    if(d != null) {
                        onHideCustomView()
                        return
                    }
                    d = view
                    uiVisibility = window.decorView.systemUiVisibility
                    h = requestedOrientation
                    a = callback
                    (window.decorView as FrameLayout).addView(d, FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
                    WindowInsetsControllerCompat(window, window.decorView).let { controller ->
                        controller.hide(WindowInsetsCompat.Type.systemBars())
                        controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    }
                }
            }
            settings.loadWithOverviewMode = true
            settings.useWideViewPort = true
            settings.javaScriptEnabled = true
            settings.userAgentString = settings.userAgentString.replace("; wv", "")
        }
    }

    private fun getId() : String {
        val shP = getSharedPreferences("BallCatchMiniGame", Context.MODE_PRIVATE)
        var id = shP.getString("id", "default") ?: "default"
        if(id == "default") {
            id = UUID.randomUUID().toString()
            shP.edit().putString("id", id).apply()
        }
        return id
    }
}