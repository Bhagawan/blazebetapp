package com.example.blazebetapp.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.blazebetapp.R
import com.example.blazebetapp.ui.theme.Grey_light
import com.example.blazebetapp.ui.theme.White_Transparent
import com.example.blazebetapp.util.Game

@Composable
fun BetSummary(sport: Int, game: Game, bet: Boolean?, onDelete: () -> Unit) {
    val sportImage = when(sport) {
        0 -> ImageVector.vectorResource(id = R.drawable.ic_football)
        1 -> ImageVector.vectorResource(id = R.drawable.ic_basketball)
        2 -> ImageVector.vectorResource(id = R.drawable.ic_cricket)
        3 -> ImageVector.vectorResource(id = R.drawable.ic_rugby)
        else -> ImageVector.vectorResource(id = R.drawable.ic_baseball)
    }
    Row(modifier = Modifier
        .fillMaxWidth().height(intrinsicSize = IntrinsicSize.Max)
        .background(White_Transparent, RoundedCornerShape(10.dp)),
        verticalAlignment = Alignment.CenterVertically) {
        Box(modifier = Modifier
            .weight(1.0f, true)
            .fillMaxHeight()
            .clickable { onDelete() }
            .background(
                White_Transparent,
                RoundedCornerShape(bottomStart = 10.dp, topStart = 10.dp)
            ), contentAlignment = Alignment.Center) {
            Image(painter = painterResource(id = R.drawable.ic_baseline_close_24), contentDescription = null, contentScale = ContentScale.Inside, modifier = Modifier.fillMaxHeight())
        }
        Column(modifier = Modifier
            .background(Color.Transparent, RoundedCornerShape(bottomEnd = 10.dp, topEnd = 10.dp))
            .fillMaxHeight()
            .weight(5.0f, true)
            .padding(5.dp), horizontalAlignment = Alignment.Start) {
            Row(modifier = Modifier.padding(5.dp), verticalAlignment = Alignment.CenterVertically) {
                Image(sportImage, contentDescription = null, contentScale = ContentScale.Fit, modifier = Modifier
                    .size(20.dp)
                    .padding(end = 3.dp))
                Text(text = game.tournament, fontSize = 10.sp, color = Color.White, textAlign = TextAlign.Center)
            }
            Text(text = game.firstName + " vs " + game.secondName, fontSize = 10.sp, color = Grey_light, textAlign = TextAlign.Start,
                modifier = Modifier.padding(5.dp))
            val coeff = when(bet) {
                true ->  game.firstCoeff
                false -> game.secondCoeff
                null -> game.drawCoeff
            }
            Text(text = coeff.toString(), fontSize = 15.sp, color = Color.White, textAlign = TextAlign.Start,
                modifier = Modifier.padding(5.dp))
        }
    }
}