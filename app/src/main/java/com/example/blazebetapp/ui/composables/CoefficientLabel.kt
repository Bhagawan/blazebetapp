package com.example.blazebetapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.blazebetapp.ui.theme.White_Transparent

@Composable
fun CoefficientLabel(label: String, coefficient: String, modifier: Modifier = Modifier, color: Color = White_Transparent) {
    Row(modifier = modifier.background(color, RoundedCornerShape(10.dp)).padding(horizontal = 15.dp, vertical = 7.dp), verticalAlignment = Alignment.CenterVertically) {
        Text(text = label, fontSize = 10.sp, color = Color.White, textAlign = TextAlign.Start, maxLines = 1, lineHeight = 15.sp, modifier = Modifier.fillMaxWidth(0.7f))
        Text(text = coefficient, fontSize = 10.sp, color = Color.White, textAlign = TextAlign.End, modifier = Modifier.weight(1.0f, true), maxLines = 1, lineHeight = 15.sp)
    }
}