package com.example.blazebetapp.ui.composables

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun DoubleTextRow(leftText: String, rightText: String, color: Color, modifier: Modifier = Modifier) {
    Row(verticalAlignment = Alignment.CenterVertically, modifier = modifier.padding(4.dp)) {
        Text(
            text = leftText,
            fontSize = 10.sp,
            color = color,
            textAlign = TextAlign.Left,
            modifier = Modifier
                .weight(1.0f, true)
        )
        Text(
            text = rightText,
            fontSize = 10.sp,
            color = color,
            textAlign = TextAlign.Right,
            modifier = Modifier
                .weight(1.0f, true)
        )
    }
}