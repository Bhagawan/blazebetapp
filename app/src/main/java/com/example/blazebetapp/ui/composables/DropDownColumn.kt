package com.example.blazebetapp.ui.composables

import androidx.compose.animation.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.blazebetapp.R

@Composable
fun DropDownColumn(label: String, modifier: Modifier = Modifier, content: @Composable ColumnScope.() -> Unit) {
    var stateOpen by remember {
        mutableStateOf(true)
    }
    Column(modifier = modifier
        .fillMaxWidth()
        .wrapContentHeight()) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clickable {
                stateOpen = !stateOpen
            }) {
            Text(label, fontSize = 15.sp, color = Color.White, textAlign = TextAlign.Start)
            val arrow = if(stateOpen) painterResource(id = R.drawable.ic_baseline_arrow_drop_up_24)
            else painterResource(id = R.drawable.ic_baseline_arrow_drop_down_24)
            Image(painter = arrow, contentDescription = null, contentScale = ContentScale.Fit, modifier = Modifier
                .size(13.dp))
            Spacer(modifier = Modifier.weight(1.0f, true))
            Image(painter = painterResource(id = R.drawable.ic_baseline_push_pin_24), contentDescription = null, contentScale = ContentScale.Fit, modifier = Modifier
                .size(13.dp)
                .align(Alignment.Top))
        }
        AnimatedVisibility(visible = stateOpen,
            enter = slideInVertically { height -> height } + expandVertically(expandFrom = Alignment.Top),
            exit =  slideOutVertically { -0 } + shrinkVertically(shrinkTowards = Alignment.Top)) {
            Column(modifier = Modifier.clipToBounds(), content = content)
        }
    }
}
