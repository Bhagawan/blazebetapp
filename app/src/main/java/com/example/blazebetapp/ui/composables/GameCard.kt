package com.example.blazebetapp.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.blazebetapp.R
import com.example.blazebetapp.ui.theme.Grey_light
import com.example.blazebetapp.ui.theme.Red
import com.example.blazebetapp.ui.theme.White_Transparent
import com.example.blazebetapp.util.Game

@Composable
fun GameCard(sport: Int, game: Game, selectedCoeffs: List<Boolean?>, modifier: Modifier = Modifier, onSelectCoeff: (outcome: Boolean?) -> Unit, onCardClick: () -> Unit ) {
    val sportImage = when(sport) {
        0 -> ImageVector.vectorResource(id = R.drawable.ic_football)
        1 -> ImageVector.vectorResource(id = R.drawable.ic_basketball)
        2 -> ImageVector.vectorResource(id = R.drawable.ic_cricket)
        3 -> ImageVector.vectorResource(id = R.drawable.ic_rugby)
        else -> ImageVector.vectorResource(id = R.drawable.ic_baseball)
    }
    Column(modifier = modifier
        .fillMaxSize()
        .background(White_Transparent, RoundedCornerShape(10.dp))
        .padding(5.dp)
        .clickable { onCardClick() }) {
        Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.Start, modifier = Modifier.padding(5.dp)) {
            Image(sportImage, contentDescription = null, contentScale = ContentScale.Fit, modifier = Modifier.size(10.dp))
            Text(text = game.tournament, fontSize = 10.sp, color = Grey_light, textAlign = TextAlign.Center, modifier = Modifier.padding(horizontal = 10.dp))
        }
        Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.Start, modifier = Modifier.padding(5.dp)) {
            Image(rememberImagePainter(game.firstLogo), contentDescription = null, contentScale = ContentScale.Fit, modifier = Modifier.size(10.dp))
            Text(text = game.firstName, fontSize = 10.sp, color = Color.White, textAlign = TextAlign.Center, modifier = Modifier.padding(horizontal = 10.dp))
        }
        Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.Start, modifier = Modifier.padding(5.dp)) {
            Image(rememberImagePainter(game.secondLogo), contentDescription = null, contentScale = ContentScale.Fit, modifier = Modifier.size(10.dp))
            Text(text = game.secondName, fontSize = 10.sp, color = Color.White, textAlign = TextAlign.Center, modifier = Modifier.padding(horizontal = 10.dp))
        }
        Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier.padding(top = 5.dp), horizontalArrangement = Arrangement.spacedBy(5.dp)) {
            CoefficientLabel(label = "1", coefficient = game.firstCoeff.toString(), Modifier.weight(1.0f, true).clickable { onSelectCoeff(true) }, if(selectedCoeffs.contains(true)) Red else White_Transparent)
            CoefficientLabel(stringResource(id = R.string.draw), coefficient = game.drawCoeff.toString(), Modifier.weight(1.0f, true).clickable { onSelectCoeff(null) }, if(selectedCoeffs.contains(null)) Red else White_Transparent)
            CoefficientLabel(label = "2", coefficient = game.secondCoeff.toString(), Modifier.weight(1.0f, true).clickable { onSelectCoeff(false) }, if(selectedCoeffs.contains(false)) Red else White_Transparent)
        }
    }
}