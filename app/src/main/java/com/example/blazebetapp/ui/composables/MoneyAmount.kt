package com.example.blazebetapp.ui.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.blazebetapp.ui.theme.Red
import com.example.blazebetapp.ui.theme.White_Transparent

@Composable
fun MoneyAmount(amount: Int, pressed: Boolean, modifier: Modifier = Modifier, onPress: () -> Unit) {
    Box(modifier = modifier.fillMaxSize().background(if (pressed) Red else White_Transparent, RoundedCornerShape(10.dp))
        .clickable { onPress() }, contentAlignment = Alignment.Center) {
        Text(text = amount.toString(),
            color = Color.White,
            textAlign = TextAlign.Center,
            fontSize = 10.sp)
    }

}