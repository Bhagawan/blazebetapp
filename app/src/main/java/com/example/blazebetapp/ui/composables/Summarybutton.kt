package com.example.blazebetapp.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.example.blazebetapp.R
import com.example.blazebetapp.ui.theme.Red

@Composable
fun SummaryButton(n: Int = 1, modifier: Modifier = Modifier, onClick: () -> Unit = {}) {
    val interactionSource = remember {
        MutableInteractionSource()
    }
    BoxWithConstraints(modifier = modifier
        .fillMaxSize()
        .clickable(interactionSource = interactionSource, null) { onClick() }
        .background(Red, CircleShape)) {
        Image(painterResource(id = R.drawable.ic_list), null, contentScale = ContentScale.Fit, modifier = Modifier
            .align(Alignment.Center)
            .padding(maxWidth / 7.0f))
        Text(text = n.toString(), fontSize = 10.sp, color = Red, textAlign = TextAlign.Center, modifier = Modifier
            .size(maxWidth / 3.0f)
            .background(Color.White, CircleShape)
            .align(
                Alignment.TopEnd
            ))
    }
}