package com.example.blazebetapp.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.blazebetapp.R
import com.example.blazebetapp.ui.composables.CoefficientLabel
import com.example.blazebetapp.ui.composables.DropDownColumn
import com.example.blazebetapp.ui.theme.Blue_dark
import com.example.blazebetapp.ui.theme.Transparent_grey
import com.example.blazebetapp.ui.theme.White_Transparent
import com.example.blazebetapp.util.Game
import com.example.casinogames.util.Navigator

@Composable
fun GameScreen(sport: Int, game: Game, scoreFirst: String = "-", scoreSecond: String = "-" ) {
    val sportImage = when(sport) {
        0 -> ImageVector.vectorResource(id = R.drawable.ic_football)
        1 -> ImageVector.vectorResource(id = R.drawable.ic_basketball)
        2 -> ImageVector.vectorResource(id = R.drawable.ic_cricket)
        3 -> ImageVector.vectorResource(id = R.drawable.ic_rugby)
        else -> ImageVector.vectorResource(id = R.drawable.ic_baseball)
    }
    Column(modifier = Modifier
        .fillMaxSize()
        .background(Blue_dark)
        .padding(horizontal = 5.dp, vertical = 20.dp)) {
        TextButton(onClick = { Navigator.navigateTo(Screens.MAIN_SCREEN) },
            shape = RoundedCornerShape(10.dp),
            colors = ButtonDefaults.buttonColors(containerColor = White_Transparent, contentColor = Color.White),
            elevation = ButtonDefaults.buttonElevation(defaultElevation = 2.dp, pressedElevation = 5.dp)) {
            Text(text = stringResource(id = R.string.btn_back), fontSize = 12.sp, color = Color.White, textAlign = TextAlign.Center)
        }

        Column(modifier = Modifier
            .wrapContentSize()
            .background(White_Transparent, RoundedCornerShape(20.dp))
            .padding(5.dp)) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start,
                modifier = Modifier
                    .padding(5.dp)
                    .fillMaxWidth()
            ) {
                Image(
                    sportImage,
                    contentDescription = null,
                    contentScale = ContentScale.Fit,
                    modifier = Modifier.size(10.dp)
                )
                Text(
                    text = game.tournament,
                    fontSize = 10.sp,
                    color = Transparent_grey,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.padding(horizontal = 10.dp)
                )
            }
            Row(modifier = Modifier
                .wrapContentHeight()
                .fillMaxWidth(), horizontalArrangement = Arrangement.Center, verticalAlignment = Alignment.CenterVertically) {
                Column(modifier = Modifier
                    .weight(1.0f, true), horizontalAlignment = Alignment.CenterHorizontally) {
                    Image(rememberImagePainter(game.firstLogo), contentDescription = null, contentScale = ContentScale.Fit, modifier = Modifier.sizeIn(maxHeight = 100.dp))
                    Text(text = game.firstName, fontSize = 20.sp, color = Color.White, textAlign = TextAlign.Center)
                }
                Row(modifier = Modifier
                    .weight(1.0f, true), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.Center) {
                    Text(text = scoreFirst, fontSize = 30.sp, color = Color.White, textAlign = TextAlign.Center, modifier = Modifier
                        .wrapContentSize()
                        .background(White_Transparent, RoundedCornerShape(3.dp))
                        .padding(4.dp))
                    Text(text = ":", fontSize = 30.sp, color = Color.White, textAlign = TextAlign.Center, modifier = Modifier.padding(3.dp))
                    Text(text = scoreSecond, fontSize = 30.sp, color = Color.White, textAlign = TextAlign.Center, modifier = Modifier
                        .wrapContentSize()
                        .background(White_Transparent, RoundedCornerShape(3.dp))
                        .padding(4.dp))
                }
                Column(modifier = Modifier
                    .weight(1.0f, true), horizontalAlignment = Alignment.CenterHorizontally) {
                    Image(rememberImagePainter(game.secondLogo), contentDescription = null, contentScale = ContentScale.Fit, modifier = Modifier.sizeIn(maxHeight = 100.dp))
                    Text(text = game.secondName, fontSize = 20.sp, color = Color.White, textAlign = TextAlign.Center)
                }
            }
        }
        DropDownColumn(label = stringResource(id = R.string.coeff_result)) {
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                CoefficientLabel(label = game.firstName, coefficient = game.firstCoeff.toString(), modifier = Modifier.weight(1.0f, true))
                CoefficientLabel(stringResource(id = R.string.draw), coefficient = game.secondCoeff.toString(), modifier = Modifier.weight(1.0f, true))
                CoefficientLabel(label = game.secondName, coefficient = game.drawCoeff.toString(), modifier = Modifier.weight(1.0f, true))
            }
        }
        DropDownColumn(label = stringResource(id = R.string.coeff_diff)) {
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                CoefficientLabel(stringResource(id = R.string.goal_diff_1), coefficient = (game.firstCoeff * 2.3f).toString(), modifier = Modifier.weight(1.0f, true))
                CoefficientLabel(stringResource(id = R.string.goal_diff_2), coefficient = (game.drawCoeff * 0.8f).toString(), modifier = Modifier.weight(1.0f, true))
            }
            Spacer(modifier = Modifier.height(10.dp))
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                CoefficientLabel(stringResource(id = R.string.goal_diff_3), coefficient = (game.firstCoeff * 0.03f).toString(), modifier = Modifier.weight(1.0f, true))
                CoefficientLabel(stringResource(id = R.string.goal_diff_4), coefficient = (game.drawCoeff * 0.01f).toString(), modifier = Modifier.weight(1.0f, true))
            }
        }
    }
}