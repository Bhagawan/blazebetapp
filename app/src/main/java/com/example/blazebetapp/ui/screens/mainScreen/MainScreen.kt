package com.example.blazebetapp.ui.screens.mainScreen

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.blazebetapp.R
import com.example.blazebetapp.ui.composables.GameCard
import com.example.blazebetapp.ui.composables.SummaryButton
import com.example.blazebetapp.ui.screens.Screens
import com.example.blazebetapp.ui.theme.Blue_dark
import com.example.blazebetapp.ui.theme.Red
import com.example.blazebetapp.ui.theme.White_Transparent
import com.example.blazebetapp.util.CurrentAppData
import com.example.blazebetapp.util.ImageMainPage
import com.example.casinogames.util.Navigator

@Preview
@Composable
fun MainScreen() {
    val viewModel = viewModel(MainScreenViewModel::class.java)
    val selectedSport by viewModel.selectedSport.collectAsState()
    val selectedCoeffs by viewModel.selectedCoeffsState.collectAsState()
    val sports = stringArrayResource(id = R.array.sports)
    val sportScrollState = rememberScrollState()

    val games = when(selectedSport) {
        0 -> CurrentAppData.footballGames
        1 -> CurrentAppData.basketballGames
        2 -> CurrentAppData.cricketGames
        3 -> CurrentAppData.rugbyGames
        else -> CurrentAppData.baseballGames
    }

    Column(modifier = Modifier
        .fillMaxSize()
        .background(Blue_dark)) {
        Image(rememberImagePainter(ImageMainPage), contentDescription = null, contentScale = ContentScale.Crop, modifier = Modifier
            .fillMaxWidth()
            .height(100.dp))
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 3.dp)
            .height(50.dp)
            .horizontalScroll(sportScrollState),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(5.dp)) {
            for(sport in sports.withIndex()) {
                val vector = when(sport.index) {
                    0 -> ImageVector.vectorResource(id = R.drawable.ic_football)
                    1 -> ImageVector.vectorResource(id = R.drawable.ic_basketball)
                    2 -> ImageVector.vectorResource(id = R.drawable.ic_cricket)
                    3 -> ImageVector.vectorResource(id = R.drawable.ic_rugby)
                    else -> ImageVector.vectorResource(id = R.drawable.ic_baseball)
                }
                val backColor = if(sport.index == selectedSport) Red else White_Transparent
                Row(modifier = Modifier
                    .clickable { viewModel.selectSport(sport.index) }
                    .background(color = backColor, shape = RoundedCornerShape(20.dp))
                    .padding(5.dp), verticalAlignment = Alignment.CenterVertically) {
                    Image(vector, contentDescription = null, contentScale = ContentScale.Fit, modifier = Modifier
                        .size(20.dp)
                        .padding(end = 3.dp))
                    Text(text = sport.value, fontSize = 14.sp, color = Color.White, textAlign = TextAlign.Center)
                }
            }
        }
        BoxWithConstraints(modifier = Modifier
            .weight(1.0f)
            .padding(horizontal = 10.dp)
            .fillMaxWidth()) {
            LazyColumn(verticalArrangement = Arrangement.spacedBy(10.dp), horizontalAlignment = Alignment.CenterHorizontally) {
                items( games ) { game ->
                    GameCard(selectedSport, game = game, selectedCoeffs.filter { it.first == game }.map { it.second }, onSelectCoeff = { outcome -> viewModel.selectCoeff(Pair(game, outcome)) }, onCardClick = {
                        CurrentAppData.selectedSport = selectedSport
                        CurrentAppData.selectedGame = game
                        Navigator.navigateTo(Screens.GAME_SCREEN)
                    })
                }
            }
        }

    }
    if(selectedCoeffs.isNotEmpty()) BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        val size = maxWidth / 7.0f
        if(selectedCoeffs.isNotEmpty()) SummaryButton(selectedCoeffs.size, modifier = Modifier
            .size(size)
            .offset(size * 3, maxHeight - size * 3), onClick = { viewModel.showSummary() })
    }

    val summaryPopup by viewModel.coefficientSummary.collectAsState()
    if(summaryPopup) SummaryPopup()
}