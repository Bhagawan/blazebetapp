package com.example.blazebetapp.ui.screens.mainScreen

import androidx.lifecycle.ViewModel
import com.example.blazebetapp.util.Game
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainScreenViewModel: ViewModel() {
    private val _selectedSport = MutableStateFlow(0)
    val selectedSport = _selectedSport.asStateFlow()

    private val _coefficientSummary = MutableStateFlow(false)
    val coefficientSummary = _coefficientSummary.asStateFlow()

    private val selectedCoeffs = ArrayList<Pair<Game, Boolean?>>()

    private val _selectedCoeffsState = MutableStateFlow(selectedCoeffs.toList())
    val selectedCoeffsState = _selectedCoeffsState.asStateFlow()

    fun selectSport(n: Int) {
        _selectedSport.tryEmit(n)
    }

    fun selectCoeff(coeff: Pair<Game, Boolean?>) {
        if(selectedCoeffs.contains(coeff)) selectedCoeffs.remove(coeff) else selectedCoeffs.add(coeff)
        _selectedCoeffsState.tryEmit(selectedCoeffs.toList())
    }

    fun showSummary() = _coefficientSummary.tryEmit(true)

    fun hideSummary() = _coefficientSummary.tryEmit(false)

    fun deleteCoeff(coeff: Pair<Game, Boolean?>) {
        if(selectedCoeffs.contains(coeff)) {
            selectedCoeffs.remove(coeff)
            _selectedCoeffsState.tryEmit(selectedCoeffs.toList())
        }
    }

    fun clearCoeffs() {
        selectedCoeffs.clear()
        _selectedCoeffsState.tryEmit(selectedCoeffs.toList())
    }

}