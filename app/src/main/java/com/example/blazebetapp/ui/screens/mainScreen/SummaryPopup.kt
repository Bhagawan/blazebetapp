package com.example.blazebetapp.ui.screens.mainScreen

import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.blazebetapp.R
import com.example.blazebetapp.ui.composables.BetSummary
import com.example.blazebetapp.ui.composables.DoubleTextRow
import com.example.blazebetapp.ui.composables.MoneyAmount
import com.example.blazebetapp.ui.theme.Blue_dark
import com.example.blazebetapp.ui.theme.Grey
import com.example.blazebetapp.ui.theme.Red
import com.example.blazebetapp.ui.theme.Transparent_grey

@Composable
fun SummaryPopup() {
    val viewModel = viewModel(MainScreenViewModel::class.java)
    val currentSport by viewModel.selectedSport.collectAsState()
    val bets by viewModel.selectedCoeffsState.collectAsState()
    val scrollState = rememberScrollState()
    var moneyAmount by remember { mutableStateOf(0) }
    val focusManager = LocalFocusManager.current
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable(MutableInteractionSource(), null) { viewModel.hideSummary() }
        .background(Transparent_grey)
        .padding(20.dp), contentAlignment = Alignment.Center) {
        Column(modifier = Modifier
            .fillMaxSize()
            .clickable { focusManager.clearFocus() }
            .background(Blue_dark, RoundedCornerShape(10.dp)), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(5.dp)) {
            Row(modifier = Modifier
                .fillMaxWidth()
                .clickable { viewModel.hideSummary() }
                .background(Red, RoundedCornerShape(topStart = 10.dp, topEnd = 10.dp))
                .padding(10.dp), verticalAlignment = Alignment.CenterVertically) {
                Image(painter = painterResource(id = R.drawable.ic_list), contentDescription = null, contentScale = ContentScale.Fit,
                    modifier = Modifier.width(20.dp))
                Text(text = stringResource(id = R.string.summary), fontSize = 18.sp, color = Color.White, textAlign = TextAlign.Start, modifier = Modifier.padding(horizontal = 5.dp))
                Text(text = bets.size.toString(), fontSize = 10.sp, color = Red, textAlign = TextAlign.Center, modifier = Modifier
                    .size(15.dp)
                    .background(Color.White, CircleShape))
                Image(painter = painterResource(id = R.drawable.ic_baseline_arrow_drop_down_24), contentDescription = null, contentScale = ContentScale.Fit,
                    modifier = Modifier.width(10.dp))
                Spacer(modifier = Modifier.weight(1.0f, true))
                Image(painter = painterResource(id = R.drawable.ic_baseline_close_24), contentDescription = null, contentScale = ContentScale.Fit,
                    modifier = Modifier.width(15.dp))
            }
            Column(modifier = Modifier
                .weight(1.0f, true)
                .padding(10.dp)
                .verticalScroll(scrollState), horizontalAlignment = Alignment.CenterHorizontally) {
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 5.dp), verticalArrangement = Arrangement.spacedBy(5.dp)) {
                    for(bet in bets) BetSummary(sport = currentSport, game = bet.first, bet = bet.second, onDelete = { viewModel.deleteCoeff(bet) })
                }
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .height(intrinsicSize = IntrinsicSize.Max)
                    .padding(vertical = 5.dp), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(3.dp)) {
                    BasicTextField(
                        value = moneyAmount.toString(),
                        onValueChange = {
                            try {
                                moneyAmount = it.toInt()
                            } catch (e: Exception) {
                            }
                        },
                        singleLine = true,
                        textStyle = TextStyle(fontSize = 14.sp, color = Color.White),
                        modifier = Modifier
                            .weight(2.0f, true),
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        decorationBox = { innerTextField ->
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(
                                        color = Blue_dark,
                                        shape = RoundedCornerShape(size = 10.dp)
                                    )
                                    .clip(RoundedCornerShape(10.dp))
                                    .border(
                                        width = 1.dp,
                                        color = Grey,
                                        shape = RoundedCornerShape(14.dp)
                                    )
                                    .padding(horizontal = 4.dp, vertical = 1.dp),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Box(modifier = Modifier.weight(1.0f, true)) {
                                    innerTextField()
                                }
                                //Spacer(modifier = Modifier.weight(1.0f, true))
                                Text(text = stringResource(id = R.string.suffix_rubel), color = Color.White, fontSize = 10.sp)
                            }
                        }
                    )
                    MoneyAmount(amount = 50, pressed = moneyAmount == 50, onPress = { moneyAmount = 50 }, modifier = Modifier
                        .weight(1.0f)
                        .fillMaxHeight()
                        .align(Alignment.CenterVertically))
                    MoneyAmount(amount = 100, pressed = moneyAmount == 100, onPress = { moneyAmount = 100 }, modifier = Modifier
                        .weight(1.0f)
                        .fillMaxHeight()
                        .align(Alignment.CenterVertically))
                    MoneyAmount(amount = 500, pressed = moneyAmount == 500, onPress = { moneyAmount = 500 }, modifier = Modifier
                        .weight(1.0f)
                        .fillMaxHeight()
                        .align(Alignment.CenterVertically))
                    MoneyAmount(amount = 1000, pressed = moneyAmount == 1000, onPress = { moneyAmount = 1000 }, modifier = Modifier
                        .weight(1.0f)
                        .fillMaxHeight()
                        .align(Alignment.CenterVertically))
                }
                val winProb = if(bets.isEmpty()) 0.0f
                else bets.fold(1.0f) {
                        s, e ->
                    val coefficient = when(e.second) {
                        true -> e.first.firstCoeff
                        false -> e.first.secondCoeff
                        null -> e.first.drawCoeff
                    }
                    s * coefficient
                }
                DoubleTextRow(leftText = stringResource(id = R.string.total_coeff), rightText = if(bets.isEmpty()) "-" else "%.2f".format(winProb), color = Grey, modifier = Modifier.wrapContentHeight())
                DoubleTextRow(leftText = stringResource(id = R.string.bet_amount), rightText = moneyAmount.toString() + stringResource(id = R.string.suffix_rubel), color = Grey, modifier = Modifier.wrapContentHeight())
                DoubleTextRow(leftText = stringResource(id = R.string.win_amount), rightText = "%.2f".format(moneyAmount * winProb) + stringResource(id = R.string.suffix_rubel), color = Color.White, modifier = Modifier.wrapContentHeight())

                TextButton(onClick = { viewModel.clearCoeffs() }) {
                    Text(text = stringResource(id = R.string.clear_coeffs), fontSize = 20.sp, color = Color.White, textAlign = TextAlign.Center, modifier = Modifier
                        .background(Grey, RoundedCornerShape(20.dp))
                        .padding(horizontal = 20.dp))
                }
                TextButton(onClick = {  }) {
                    Text(text = stringResource(id = R.string.accept_bet), fontSize = 20.sp, color = Color.White, textAlign = TextAlign.Center, modifier = Modifier
                        .background(Red, RoundedCornerShape(20.dp))
                        .padding(horizontal = 20.dp))
                }
            }
        }
    }
}