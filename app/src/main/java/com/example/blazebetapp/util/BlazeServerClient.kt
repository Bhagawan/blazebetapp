package com.example.blazebetapp.util

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface BlazeServerClient {

    @FormUrlEncoded
    @POST(UrlSplash)
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<BlazeSplashResponse>


    @GET("BlazeBetApp/{name}")
    suspend fun getGamesList(@Path("name") filename: String) : Response<List<Game>>

    companion object {
        fun create() : BlazeServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(BlazeServerClient::class.java)
        }
    }

}