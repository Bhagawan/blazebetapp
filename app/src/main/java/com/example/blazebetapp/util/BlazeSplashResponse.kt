package com.example.blazebetapp.util

import androidx.annotation.Keep

@Keep
data class BlazeSplashResponse(val url : String)