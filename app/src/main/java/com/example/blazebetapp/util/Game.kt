package com.example.blazebetapp.util

import androidx.annotation.Keep

@Keep
data class Game(val tournament: String,
    val time: String,
    val firstLogo: String,
    val firstName: String,
    val secondLogo: String,
    val secondName: String,
    val firstCoeff: Float,
    val drawCoeff: Float,
    val secondCoeff: Float)
