package com.example.blazebetapp.util

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.blazebetapp.ui.screens.GameScreen
import com.example.blazebetapp.ui.screens.Screens
import com.example.blazebetapp.ui.screens.SplashScreen
import com.example.blazebetapp.ui.screens.WebViewScreen
import com.example.blazebetapp.ui.screens.mainScreen.MainScreen
import com.example.casinogames.util.Navigator
import im.delight.android.webview.AdvancedWebView
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            SplashScreen()
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.onBackPressed()) exitProcess(0)
            }
            WebViewScreen(webView, remember { CurrentAppData.url })
        }
        composable(Screens.MAIN_SCREEN.label) {
            BackHandler(true) {}
            MainScreen()
        }
        composable(Screens.GAME_SCREEN.label) {
            BackHandler(true) {}
            GameScreen(CurrentAppData.selectedSport, CurrentAppData.selectedGame)
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    navController.navigate(currentScreen.label)

    navController.enableOnBackPressed(true)
}