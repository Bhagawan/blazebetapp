package com.example.blazebetapp.util

const val UrlSplash = "BlazeBetApp/splash.php"
const val UrlLogo = "http://195.201.125.8/BlazeBetApp/logo.png"
const val ImageMainPage = "http://195.201.125.8/BlazeBetApp/gate.png"
const val FileFootballList = "Football.json"
const val FileBasketballList = "Basketball.json"
const val FileCricketList = "Cricket.json"
const val FileRugbyList = "Rugby.json"
const val FileBaseballList = "Baseball.json"